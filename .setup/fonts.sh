    # Fonts {{{
    brew tap caskroom/fonts   
    brew cask install font-source-code-pro
    brew cask install font-source-code-pro-for-powerline
    brew cask install font-inconsolata-for-powerline
    brew cask install font-inconsolata
        # }}}